# README #

This README should contain all relevant to the MFSSIA-API information

### What is this API for? ###

* Quick summary

This is an Rest-API that should serve as a bridge between smart contracts and oracles on one side and DKG node data cluster on another side. 

* Version 0.0.1
 
Initial commit with mocked DKG 

* Version 0.0.2

Added support for real DKG call

* Version 0.0.3

Added publising of contract,gateways,securityLicense andsystem. 

Added initial setup on DKG node to perform basic tests of REST

### How do I get set up? ###

* Summary of set up for local machine

Simply run the bootable mfssia-api.jar.

* Configuration

No special configuration needed for now

* Dependencies

The program will depend on preconfigured DKG node data cluster that it will query with requests

* Database configuration

No database planed

* How to run tests

Junit tests are run are run automatically on compilation

* Deployment instructions

For now the program is deployed on amazon elastic beanstalk. No special knowledge needed. 
The url to test API can be accessed via swagger http://mfssia-api.eu-central-1.elasticbeanstalk.com/swagger-ui/index.html

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact