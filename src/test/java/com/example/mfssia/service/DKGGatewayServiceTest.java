package com.example.mfssia.service;

import com.example.mfssia.model.latesttrail.*;
import com.example.mfssia.utils.IdentifierTypes;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
class DKGGatewayServiceTest {

    private static final String CONTRACT_ID = "1234";
    private static final String NETWORK_1 = "ethereum";
    private static final String NETWORK_2 = "polygon";

    @InjectMocks
    DKGGatewayService service;

    @Mock
    DKGConnector dkgConnector;

    @Mock
    DKGContractService contractService;

    private OtObject createDKGResponseForContract(){
        Properties properties = Properties.builder().timestamp("2021-03-24T16:48:05").producerNetwork(NETWORK_1).consumerNetwork(NETWORK_2).build();
        Identifier identifier = Identifier.builder().identifierType(IdentifierTypes.CONTRACT.toString()).identifierValue(CONTRACT_ID).build();
        return OtObject.builder().identifiers(new ArrayList<>(Collections.singletonList(identifier))).properties(properties).build();

    }

    private ArrayList<Graph> createDKGResponseForGateways(){
        ArrayList<ActiveGateway> activeGatewaysList = new ArrayList<>();
        activeGatewaysList.add(ActiveGateway.builder().producerNetwork(NETWORK_1).consumerNetwork(NETWORK_2).build());
        Properties properties = Properties.builder().timestamp("2021-03-24T16:48:05").activeGateways(activeGatewaysList).build();
        Identifier identifier = Identifier.builder().identifierType(IdentifierTypes.ID.toString()).identifierValue("gateways").build();
        OtObject otObject = OtObject.builder().identifiers(new ArrayList<>(Collections.singletonList(identifier))).properties(properties).build();
        Graph graph = Graph.builder().datasets(new ArrayList<>(Collections.singletonList("dataset"))).otObject(otObject).build();

        ArrayList<Graph> graphList = new ArrayList<>();
        graphList.add(graph);
        return graphList;
    }

    @Test
    public void testNullInputArg() {
        boolean result = service.checkGateways("");
        assertFalse(result);
    }

    @Test
    public void testActiveGatewaysTrue() {
        when(contractService.getLatestContractById(CONTRACT_ID)).thenReturn(createDKGResponseForContract());
        when(dkgConnector.sendDKGQueryForGateways()).thenReturn(createDKGResponseForGateways());
        when(dkgConnector.checkDatasetValidity(anyString())).thenReturn(true);
        boolean result = service.checkGateways(CONTRACT_ID);
        assertTrue(result);
    }

    @Test
    public void testActiveGatewaysFalse() {
        boolean result = service.checkGateways("12369");
        assertFalse(result);
    }

}