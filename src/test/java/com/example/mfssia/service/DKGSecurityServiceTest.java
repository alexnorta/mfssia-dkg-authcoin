package com.example.mfssia.service;

import com.example.mfssia.model.latesttrail.Graph;
import com.example.mfssia.model.latesttrail.Identifier;
import com.example.mfssia.model.latesttrail.OtObject;
import com.example.mfssia.model.latesttrail.Properties;
import com.example.mfssia.utils.IdentifierTypes;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
class DKGSecurityServiceTest {

    private static final String OWNER_1 = "12388";
    private static final String OWNER_2 = "12333";
    private static final String VALIDITY = "23-05-2033";

    @InjectMocks
    DKGSecurityService service;

    @Mock
    DKGConnector dkgConnector;

    private ArrayList<Graph> createDKGResponseForSL1() {
        Properties properties1 = Properties.builder().owner(OWNER_1).validity(VALIDITY).build();
        Identifier identifier1 = Identifier.builder().identifierType(IdentifierTypes.LICENSE.toString()).identifierValue("license_1").build();
        OtObject otObject1 = OtObject.builder().identifiers(new ArrayList<>(Collections.singletonList(identifier1))).properties(properties1).build();
        Graph graph1 = Graph.builder().datasets(new ArrayList<>(Collections.singletonList("dataset"))).datasets(new ArrayList<>(Collections.singletonList("dataset"))).otObject(otObject1).build();

        ArrayList<Graph> graphList = new ArrayList<>();
        graphList.add(graph1);
        return graphList;
    }


    private ArrayList<Graph> createDKGResponseForSL2() {
        Properties properties2 = Properties.builder().owner(OWNER_2).validity(VALIDITY).build();
        Identifier identifier2 = Identifier.builder().identifierType(IdentifierTypes.LICENSE.toString()).identifierValue("license_2").build();
        OtObject otObject2 = OtObject.builder().identifiers(new ArrayList<>(Collections.singletonList(identifier2))).properties(properties2).build();
        Graph graph2 = Graph.builder().datasets(new ArrayList<>(Collections.singletonList("dataset"))).datasets(new ArrayList<>(Collections.singletonList("dataset"))).otObject(otObject2).build();

        ArrayList<Graph> graphList = new ArrayList<>();

        graphList.add(graph2);
        return graphList;
    }


    @Test
    public void testNullInputArg1() throws ParseException {
        boolean result = service.checkLicense("", OWNER_2);
        assertFalse(result);
    }

    @Test
    public void testNullInputArg2() throws ParseException {
        boolean result = service.checkLicense(OWNER_1, "");
        assertFalse(result);
    }

    @Test
    public void testLicenseAreTrue() throws ParseException {
        when(dkgConnector.sendDKGQueryForSecurityLicense(OWNER_1)).thenReturn(createDKGResponseForSL1());
        when(dkgConnector.sendDKGQueryForSecurityLicense(OWNER_2)).thenReturn(createDKGResponseForSL2());
        when(dkgConnector.checkDatasetValidity(anyString())).thenReturn(true);
        boolean result = service.checkLicense(OWNER_1, OWNER_2);
        assertTrue(result);
    }

}