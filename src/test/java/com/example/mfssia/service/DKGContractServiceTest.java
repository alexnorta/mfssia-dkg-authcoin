package com.example.mfssia.service;

import com.example.mfssia.model.latesttrail.Graph;
import com.example.mfssia.model.latesttrail.Identifier;
import com.example.mfssia.model.latesttrail.OtObject;
import com.example.mfssia.model.latesttrail.Properties;
import com.example.mfssia.utils.IdentifierTypes;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collections;

import static com.example.mfssia.utils.ConstantsString.NO_HASH;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
class DKGContractServiceTest {

    private static final String CONTRACT_ID = "1234";

    @InjectMocks
    DKGContractService service;

    @Mock
    DKGConnector dkgConnector;

    private ArrayList<Graph> createDKGResponseForContract() {
        Properties properties = Properties.builder().price(10000).quantity(2).deliveryInterval(3).productName("Apple").timestamp("2021-03-24T16:48:05").build();
        Identifier identifier = Identifier.builder().identifierType(IdentifierTypes.CONTRACT.toString()).identifierValue(CONTRACT_ID).build();

        OtObject otObject = OtObject.builder().identifiers(new ArrayList<>(Collections.singletonList(identifier))).properties(properties).build();
        Graph graph = Graph.builder().datasets(new ArrayList<>(Collections.singletonList("dataset"))).otObject(otObject).build();

        Properties properties2 = Properties.builder().price(10000).quantity(2).deliveryInterval(1).productName("Apple").timestamp("2020-03-25T16:48:05").build();
        Identifier identifier2 = Identifier.builder().identifierType(IdentifierTypes.CONTRACT.toString()).identifierValue(CONTRACT_ID).build();

        OtObject otObject2 = OtObject.builder().identifiers(new ArrayList<>(Collections.singletonList(identifier2))).properties(properties2).build();
        Graph graph2 = Graph.builder().datasets(new ArrayList<>(Collections.singletonList("dataset"))).otObject(otObject2).build();

        ArrayList<Graph> graphList = new ArrayList<>();
        graphList.add(graph);
        graphList.add(graph2);
        return graphList;
    }

    @Test
    public void testNullInput() {
        String contractHash = service.getContractHash("");
        Assert.assertEquals(contractHash, NO_HASH);
    }

    @Test
    public void testNoGraphFound() {
        String contractHash = service.getContractHash("23");
        Assert.assertEquals(contractHash, NO_HASH);
    }

    @Test
    public void testHashGeneration() {
        when(dkgConnector.sendDKGQueryForContracts(CONTRACT_ID)).thenReturn(createDKGResponseForContract());
        when(dkgConnector.checkDatasetValidity(anyString())).thenReturn(true);
        String contractHash = service.getContractHash(CONTRACT_ID);
        Assert.assertEquals(contractHash, "3636a05186cdf92f726fc5c9c2c9f961ae9b9c48f7ab841723fc539c10fe7f77");
    }
}