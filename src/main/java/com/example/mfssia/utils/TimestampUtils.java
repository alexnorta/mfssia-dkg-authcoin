package com.example.mfssia.utils;

import com.example.mfssia.model.latesttrail.OtObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimestampUtils {

    public static final String TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    public static final OtObject findLatestObject( OtObject latestOtObject, OtObject newOtObject){
        if (latestOtObject == null) {
            return newOtObject;
        } else {
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat(TIMESTAMP_FORMAT);
                Date newDate = dateFormat.parse(newOtObject.getProperties().getTimestamp());
                Date latestDate = dateFormat.parse(latestOtObject.getProperties().getTimestamp());
                if (newDate.after(latestDate)) {
                    return newOtObject;
                } else return latestOtObject;

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return latestOtObject;
    }

    public static final String getCurrentTimestamp(){
        return  new SimpleDateFormat(TIMESTAMP_FORMAT).format(new Date(System.currentTimeMillis()));
    }
}
