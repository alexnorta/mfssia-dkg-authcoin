package com.example.mfssia.utils;

public enum IdentifierTypes {

    ID("id"),
    CONTRACT("contract"),
    GATEWAYS("gateways"),
    SYSTEM("system"),
    NETWORKS("networks"),
    LICENSE("license");

    private final String stringValue;

    IdentifierTypes(final String s) {
        stringValue = s;
    }

    public String toString() {
        return stringValue;
    }
}
