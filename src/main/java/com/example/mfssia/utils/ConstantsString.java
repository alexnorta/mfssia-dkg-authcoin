package com.example.mfssia.utils;

public class ConstantsString {

    public static final String NO_HASH = "no_hash";
    public static final String ERROR_PUBLISH = "no_hash";
    public static final String SETUP_DONE = "Initial data imported";
    public static final String SERVICE_PROVIDER = "service_provider";
    public static final String SERVICE_CONSUMER = "service_consumer";

    public static final String API_LATEST_TRAIL = "/api/latest/trail";
    public static final String DATASET_INFO = "/api/latest/get_dataset_info/";
    public static final String PUBLISH_DATASET = "/api/latest/import";
}
