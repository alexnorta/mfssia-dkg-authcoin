package com.example.mfssia.utils;

import com.example.mfssia.model.datasetinfo.DatasetInfoRoot;
import com.example.mfssia.model.latesttrail.Graph;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class DKGResponseParser {

    private static final int END_CUT_NUMBER = 2;
    private static final int MIDDLE_CUT_NUMBER = 11;

    public static final ArrayList<Graph> convertJSONResponseToObjects(String body){
        int count = 0;
        ArrayList responseList = new ArrayList<Graph>();
        if (StringUtils.isBlank(body)) return responseList;

        String[] datasets = body.split("datasets");
        if (datasets.length == 1) return responseList;
        List<String> stringList =  new LinkedList<>(Arrays.asList(datasets));
        stringList.remove(0);
        for (String dataset : stringList) {
            count++;
            String correctDataset = "{\"datasets" + dataset;
            if (stringList.size() != count) {
                correctDataset = correctDataset.substring(0, correctDataset.length() - MIDDLE_CUT_NUMBER);
            } else {
                correctDataset = correctDataset.substring(0, correctDataset.length() - END_CUT_NUMBER);
            }

            try {
                ObjectMapper mapper = new ObjectMapper();
                mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
                Graph node = mapper.readValue(correctDataset, Graph.class);
                responseList.add(node);
            } catch (JsonProcessingException e) {
                System.out.println(e);
            }
        }

        return responseList;
    }

    public static final DatasetInfoRoot getDatasetInfoJson(String body){
        ObjectMapper mapper = new ObjectMapper();
        DatasetInfoRoot datasetInfoRoot = null;
        mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        try {
            datasetInfoRoot = mapper.readValue(body, DatasetInfoRoot.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return datasetInfoRoot;
    }
}
