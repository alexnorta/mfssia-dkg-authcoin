package com.example.mfssia.service;

import com.example.mfssia.model.latesttrail.ActiveGateway;
import com.example.mfssia.model.latesttrail.OtObject;
import com.example.mfssia.model.publish.ContractData;
import com.example.mfssia.model.publish.GatewayData;
import com.example.mfssia.model.publish.LicenseData;
import com.example.mfssia.model.publish.SystemData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.mfssia.utils.ConstantsString.ERROR_PUBLISH;
import static com.example.mfssia.utils.ConstantsString.SETUP_DONE;
import static com.example.mfssia.utils.TimestampUtils.getCurrentTimestamp;

@Service
public class DKGInitialSetupService {

    private final DKGConnector dkgConnector;
    private final DKGContractService contractService;

    @Value("classpath:DKGPublish/contract.json")
    Resource contract;
    @Value("classpath:DKGPublish/gateways.json")
    Resource gateways;
    @Value("classpath:DKGPublish/securityLicense1.json")
    Resource securityLicense1;
    @Value("classpath:DKGPublish/securityLicense2.json")
    Resource securityLicense2;
    @Value("classpath:DKGPublish/system1.json")
    Resource system1;
    @Value("classpath:DKGPublish/system2.json")
    Resource system2;
    @Value("classpath:DKGPublish/types.json")
    Resource types;

    @Autowired
    public DKGInitialSetupService(DKGConnector dkgConnector, DKGContractService contractService) {
        this.dkgConnector = dkgConnector;
        this.contractService = contractService;
    }

    public String publishFiles() throws IOException {
        OtObject latestContractById = contractService.getLatestContractById("1234");
        if (latestContractById != null) return SETUP_DONE;

        dkgConnector.publishJsonBody(insertTimestamp(loadTemplate(types)));
        dkgConnector.publishJsonBody(insertTimestamp(loadTemplate(gateways)));
        dkgConnector.publishJsonBody(insertTimestamp(loadTemplate(system1)));
        dkgConnector.publishJsonBody(insertTimestamp(loadTemplate(system2)));
        dkgConnector.publishJsonBody(insertTimestamp(loadTemplate(securityLicense1)));
        dkgConnector.publishJsonBody(insertTimestamp(loadTemplate(securityLicense2)));
        dkgConnector.publishJsonBody(insertTimestamp(loadTemplate(contract)));

        return SETUP_DONE;
    }

    private String insertTimestamp(String templateBody) {
        Map<String, String> valuesMap = new HashMap<>();

        valuesMap.put("timestamp", getCurrentTimestamp());

        return replaceValuesInTemplate(valuesMap, templateBody);
    }

    private String replaceValuesInTemplate(Map<String, String> valuesMap, String templateString) {
        StringSubstitutor sub = new StringSubstitutor(valuesMap);
        return sub.replace(templateString);
    }


    private String loadTemplate(Resource file) throws IOException {
        return IOUtils.toString(file.getInputStream(), StandardCharsets.UTF_8.name());
    }

}
