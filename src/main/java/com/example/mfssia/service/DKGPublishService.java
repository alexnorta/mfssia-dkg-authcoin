package com.example.mfssia.service;

import com.example.mfssia.model.latesttrail.ActiveGateway;
import com.example.mfssia.model.publish.ContractData;
import com.example.mfssia.model.publish.GatewayData;
import com.example.mfssia.model.publish.LicenseData;
import com.example.mfssia.model.publish.SystemData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.mfssia.utils.ConstantsString.ERROR_PUBLISH;
import static com.example.mfssia.utils.TimestampUtils.getCurrentTimestamp;

@Service
public class DKGPublishService {

    private final DKGConnector dkgConnector;

    @Value("classpath:DKGPublishTemplate/contract.json")
    Resource contractTemplate;

    @Value("classpath:DKGPublishTemplate/securityLicense.json")
    Resource securityLicenseTemplate;

    @Value("classpath:DKGPublishTemplate/system.json")
    Resource systemTemplate;

    @Value("classpath:DKGPublishTemplate/gateways.json")
    Resource gatewayTemplate;

    @Autowired
    public DKGPublishService(DKGConnector dkgConnector) {
        this.dkgConnector = dkgConnector;
    }

    private static final String DELIMITER = ";";

    public String publishGateways(GatewayData gatewayData) throws IOException {
        String file = loadTemplate(gatewayTemplate);
        if (StringUtils.isBlank(file) || CollectionUtils.isEmpty(gatewayData.getActiveGateways())) return ERROR_PUBLISH;
        String jsonBodyWithValues = insertGateway(file, gatewayData);

        return dkgConnector.publishJsonBody(jsonBodyWithValues);
    }

    public String publishContractData(ContractData contractData) throws IOException {
        String file = loadTemplate(contractTemplate);
        if (StringUtils.isBlank(file)) return ERROR_PUBLISH;
        String jsonBodyWithValues = insertContractValues(file, contractData);

        return dkgConnector.publishJsonBody(jsonBodyWithValues);
    }

    private String insertContractValues(String templateBody, ContractData contractData) {

        Map<String, String> valuesMap = new HashMap<>();

        valuesMap.put("contractNumber", contractData.getContractNumber());
        valuesMap.put("timestamp", getCurrentTimestamp());
        valuesMap.put("consumerNetwork", contractData.getConsumerNetwork());
        valuesMap.put("producerNetwork", contractData.getProducerNetwork());
        valuesMap.put("price", contractData.getPrice());
        valuesMap.put("quantity", contractData.getQuantity());
        valuesMap.put("deliveryInterval", contractData.getDeliveryInterval());
        valuesMap.put("productName", contractData.getProductName());

        return replaceValuesInTemplate(valuesMap, templateBody);
    }

    public String publishSystemData(SystemData systemData) throws IOException {
        String file = loadTemplate(systemTemplate);
        if (StringUtils.isBlank(file)) return ERROR_PUBLISH;
        String jsonBodyWithValues = insertSystemValues(file, systemData);

        return dkgConnector.publishJsonBody(jsonBodyWithValues);
    }

    private String insertSystemValues(String templateBody, SystemData systemData) {
        Map<String, String> valuesMap = new HashMap<>();

        valuesMap.put("systemNumber", systemData.getSystemNumber());
        valuesMap.put("timestamp", getCurrentTimestamp());
        valuesMap.put("network", systemData.getNetwork());

        return replaceValuesInTemplate(valuesMap, templateBody);
    }

    public String publishLicenseData(LicenseData licenseData) throws IOException {
        String file = loadTemplate(securityLicenseTemplate);
        if (StringUtils.isBlank(file)) return ERROR_PUBLISH;
        String jsonBodyWithValues = insertLicenseValues(file, licenseData);

        return dkgConnector.publishJsonBody(jsonBodyWithValues);
    }

    private String insertLicenseValues(String templateBody, LicenseData licenseData) {
        Map<String, String> valuesMap = new HashMap<>();
        valuesMap.put("systemNumber", licenseData.getSystemNumber());
        valuesMap.put("licenseNumber", licenseData.getLicenseNumber());
        valuesMap.put("timestamp", getCurrentTimestamp());
        valuesMap.put("issuer", licenseData.getIssuer());
        valuesMap.put("validity", licenseData.getValidity());

        return replaceValuesInTemplate(valuesMap, templateBody);
    }


    private String insertGateway(String templateBody, GatewayData gatewayData) {
        Map<String, String> valuesMap = new HashMap<>();

        valuesMap.put("timestamp", getCurrentTimestamp());
        valuesMap.put("gateways", generateActiveGatewaysJsonString(gatewayData.getActiveGateways()));

        return replaceValuesInTemplate(valuesMap, templateBody);
    }

    private String generateActiveGatewaysJsonString(List<ActiveGateway> gatewayData){
       String returnGatewayPairsBody = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            returnGatewayPairsBody = mapper.writeValueAsString(gatewayData);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return returnGatewayPairsBody;
    }

    private String replaceValuesInTemplate(Map<String, String> valuesMap, String templateString) {
        StringSubstitutor sub = new StringSubstitutor(valuesMap);
        return sub.replace(templateString);
    }

    private String loadTemplate(Resource file) throws IOException {
        return IOUtils.toString(file.getInputStream(), StandardCharsets.UTF_8.name());
    }

}
