package com.example.mfssia.service;

import com.example.mfssia.model.datasetinfo.DataProviderWallet;
import com.example.mfssia.model.datasetinfo.DatasetInfoRoot;
import com.example.mfssia.model.latesttrail.Graph;
import com.example.mfssia.utils.IdentifierTypes;
import net.minidev.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

import static com.example.mfssia.utils.ConstantsString.*;
import static com.example.mfssia.utils.DKGResponseParser.convertJSONResponseToObjects;
import static com.example.mfssia.utils.DKGResponseParser.getDatasetInfoJson;

@Service
public class DKGConnector {

    @Value("${dkg.url}" + API_LATEST_TRAIL)
    String apiLatestTrailURL;

    @Value("${dkg.url}" + DATASET_INFO)
    String datasetInfoURL;


    @Value("${dkg.url}" + PUBLISH_DATASET)
    String publishDatasetURL;

    @Value("${dkg.dataProviderWallet}")
    String providerWallet;

    public ArrayList<Graph> sendDKGQueryForGateways() {

        ArrayList<String> identifierTypesList = new ArrayList<>();
        identifierTypesList.add(IdentifierTypes.ID.toString());

        ArrayList<String> identifierValuesList = new ArrayList<>();
        identifierValuesList.add(IdentifierTypes.GATEWAYS.toString());

        JSONObject queryJsonObject = new JSONObject();
        queryJsonObject.put("identifier_types", identifierTypesList);
        queryJsonObject.put("identifier_values", identifierValuesList);
        queryJsonObject.put("connection_types", new ArrayList<>());
        queryJsonObject.put("depth", 10);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(queryJsonObject.toString(), headers);

        ResponseEntity<String> result = new RestTemplate().postForEntity(apiLatestTrailURL, entity, String.class);
        return  convertJSONResponseToObjects(result.getBody()) ;
    }

    public ArrayList<Graph> sendDKGQueryForContracts(String contractId) {

        ArrayList<String> identifierTypesList = new ArrayList<>();
        identifierTypesList.add(IdentifierTypes.CONTRACT.toString());

        ArrayList<String> identifierValuesList = new ArrayList<>();
        identifierValuesList.add(contractId);

        JSONObject queryJsonObject = new JSONObject();
        queryJsonObject.put("identifier_types", identifierTypesList);
        queryJsonObject.put("identifier_values", identifierValuesList);
        queryJsonObject.put("connection_types", new ArrayList<>());
        queryJsonObject.put("depth", 10);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(queryJsonObject.toString(), headers);

        ResponseEntity<String> result = new RestTemplate().postForEntity(apiLatestTrailURL, entity, String.class);
        return  convertJSONResponseToObjects(result.getBody()) ;
    }

    public ArrayList<Graph> sendDKGQueryForSecurityLicense(String licenseOwner) {

        ArrayList<String> identifierTypesList = new ArrayList<>();
        identifierTypesList.add(IdentifierTypes.SYSTEM.toString());

        ArrayList<String> identifierValuesList = new ArrayList<>();
        identifierValuesList.add(licenseOwner);

        JSONObject queryJsonObject = new JSONObject();
        queryJsonObject.put("identifier_types", identifierTypesList);
        queryJsonObject.put("identifier_values", identifierValuesList);
        queryJsonObject.put("connection_types", new ArrayList<>());
        queryJsonObject.put("depth", 10);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(queryJsonObject.toString(), headers);

        ResponseEntity<String> result = new RestTemplate().postForEntity(apiLatestTrailURL, entity, String.class);
        return  convertJSONResponseToObjects(result.getBody()) ;
    }

    public boolean checkDatasetValidity(String dataset) {
        if (StringUtils.isBlank(dataset)) return false;
        ResponseEntity<String> result = new RestTemplate().getForEntity(datasetInfoURL +dataset, String.class);

        DatasetInfoRoot datasetInfoJson = getDatasetInfoJson(result.getBody());
        ArrayList<DataProviderWallet> dataProviderWallets = datasetInfoJson.getDataProviderWallets();

        if (dataProviderWallets == null || dataProviderWallets.size() == 0) return false;

        return  StringUtils.equalsIgnoreCase(dataProviderWallets.iterator().next().getWallet(), providerWallet);
    }

    public String publishJsonBody(String jsonBody) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, String> fileMap = new LinkedMultiValueMap<>();
        ContentDisposition contentDisposition = ContentDisposition
                .builder("form-data")
                .name("file")
                .filename("publish.json")
                .build();
        fileMap.add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString());
        HttpEntity<byte[]> fileEntity = new HttpEntity<>(jsonBody.getBytes(), fileMap);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("standard_id", "GRAPH");
        body.add("file", fileEntity);

        HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(body, headers);

        ResponseEntity<String> result = new RestTemplate().postForEntity(publishDatasetURL, entity, String.class);
        return  result.getBody();
    }
}
