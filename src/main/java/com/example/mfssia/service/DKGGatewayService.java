package com.example.mfssia.service;

import com.example.mfssia.model.latesttrail.*;
import com.example.mfssia.utils.IdentifierTypes;
import com.example.mfssia.utils.TimestampUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class DKGGatewayService {

    private final DKGConnector dkgConnector;
    private final DKGContractService contractService;


    @Autowired
    public DKGGatewayService(DKGConnector dkgConnector, DKGContractService contractService) {
        this.dkgConnector = dkgConnector;
        this.contractService = contractService;
    }

    public boolean checkGateways(String contractId) {
        if (StringUtils.isBlank(contractId)) return false;

        OtObject contract = contractService.getLatestContractById(contractId);

        if (contract == null) return false;

        Properties properties = contract.getProperties();
        if (properties == null) return false;

        String gatewayOfProducer = properties.getProducerNetwork();
        String gatewayOfConsumer = properties.getConsumerNetwork();
        if (StringUtils.isBlank(gatewayOfProducer) || StringUtils.isBlank(gatewayOfConsumer)) return false;

        ArrayList<Graph> gateWayList = queryGateway();
        if (gateWayList == null) return false;

        OtObject gatewaysData = findGateway(gateWayList);
        if (gatewaysData == null) return false;

        return checkNetworks(gatewaysData, gatewayOfProducer, gatewayOfConsumer);
    }

    private ArrayList<Graph> queryGateway() {
        ArrayList<Graph> arrayList = dkgConnector.sendDKGQueryForGateways();
        if (arrayList != null && arrayList.size() > 0) {
            return arrayList;
        }
        return null;
    }

    //this method is redundant since it does same thing as query but i leave it here for the future when we will may be use more sophisticated filtering
    private OtObject findGateway(ArrayList<Graph> gateWayOtObjectArray) {
        OtObject returnedGateway = null;
        for (Graph graph : gateWayOtObjectArray) {
            OtObject otObject = graph.getOtObject();
            if (otObject != null) {
                for (Identifier identifier : otObject.getIdentifiers()) {
                    if (identifier != null) {
                        if (StringUtils.equalsIgnoreCase(IdentifierTypes.ID.toString(), identifier.identifierType) &&
                                StringUtils.equalsIgnoreCase(IdentifierTypes.GATEWAYS.toString(), identifier.identifierValue)) {
                            if (StringUtils.isNotBlank(otObject.getProperties().getTimestamp())
                                    && dkgConnector.checkDatasetValidity(graph.getDatasets().iterator().next())) {
                                returnedGateway = TimestampUtils.findLatestObject(returnedGateway, otObject);
                            }
                        }
                    }
                }
            }
        }
        return returnedGateway;
    }

    private boolean checkNetworks(OtObject otObject, String gatewayOfProducer, String gatewayOfConsumer) {
        boolean gate1Check = false;
        boolean gate2Check = false;

        ArrayList<ActiveGateway> gateways = otObject.getProperties().getActiveGateways();
        if (gateways != null) {
            for (ActiveGateway gateway : gateways) {
                if (StringUtils.equalsIgnoreCase(gatewayOfProducer, gateway.getProducerNetwork())) {
                    gate1Check = true;
                }
                if (StringUtils.equalsIgnoreCase(gatewayOfConsumer, gateway.getConsumerNetwork())) {
                    gate2Check = true;
                }

                if (gate1Check && gate2Check) {
                    return true;
                } else {
                    gate1Check = false;
                    gate2Check = false;
                }
            }

        }

        return false;
    }
}
