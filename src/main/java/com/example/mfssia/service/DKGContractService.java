package com.example.mfssia.service;

import com.example.mfssia.model.latesttrail.Graph;
import com.example.mfssia.model.latesttrail.Identifier;
import com.example.mfssia.model.latesttrail.OtObject;
import com.example.mfssia.model.latesttrail.Properties;
import com.example.mfssia.utils.IdentifierTypes;
import com.example.mfssia.utils.TimestampUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

import static com.example.mfssia.utils.ConstantsString.NO_HASH;

@Service
public class DKGContractService {

    private final DKGConnector dkgConnector;

    @Autowired
    public DKGContractService(DKGConnector dkgConnector) {
        this.dkgConnector = dkgConnector;
    }

    private static final String DELIMITER = ";";

    public String getContractHash(String contractId) {
        if (StringUtils.isBlank(contractId)) return NO_HASH;
        OtObject contract = getLatestContractById(contractId);
        if (contract == null) return NO_HASH;
        return generateHash(contract);
    }

    OtObject getLatestContractById(String contractId) {
        ArrayList<Graph> contractList = queryContract(contractId);
        if (contractList == null) return null;

        return findContract(contractList, contractId);
    }

    private String generateHash(OtObject otObject) {
        Properties props = otObject.getProperties();
        String hashString = String.format("%d" + DELIMITER + "%d" + DELIMITER + "%d" + DELIMITER + "%s",
                props.getPrice(), props.getQuantity(), props.getDeliveryInterval(), props.getProductName().toLowerCase());
        return DigestUtils.sha256Hex(hashString);
    }

    private ArrayList<Graph> queryContract(String contractId) {
        ArrayList<Graph> arrayList = dkgConnector.sendDKGQueryForContracts(contractId);
        if (arrayList != null && arrayList.size() > 0) {
            return arrayList;
        }
        return null;
    }

    //this method is redundant since it does same thing as query but i leave it here for the future when we will may be use more sophisticated filtering
    private OtObject findContract(ArrayList<Graph> contractOtObjectArray, String contractId) {
        OtObject returnedContract = null;
        for (Graph graph : contractOtObjectArray) {
            OtObject otObject = graph.getOtObject();
            if (otObject != null) {
                for (Identifier identifier : otObject.getIdentifiers()) {
                    if (identifier != null) {
                        if (StringUtils.equalsIgnoreCase(IdentifierTypes.CONTRACT.toString(), identifier.identifierType) && otObject.getProperties() != null) {
                            String timestamp = otObject.getProperties().getTimestamp();
                            if (StringUtils.equalsIgnoreCase(identifier.getIdentifierValue(), contractId) && StringUtils.isNotBlank(timestamp)
                                    && dkgConnector.checkDatasetValidity(graph.getDatasets().iterator().next())) {
                                returnedContract = TimestampUtils.findLatestObject(returnedContract, otObject);

                            }
                        }
                    }
                }
            }
        }
        return returnedContract;
    }
}
