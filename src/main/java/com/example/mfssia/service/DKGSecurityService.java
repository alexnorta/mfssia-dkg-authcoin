package com.example.mfssia.service;

import com.example.mfssia.model.latesttrail.Graph;
import com.example.mfssia.model.latesttrail.Identifier;
import com.example.mfssia.model.latesttrail.OtObject;
import com.example.mfssia.utils.IdentifierTypes;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

@Service
public class DKGSecurityService {

    private final DKGConnector dkgConnector;

    @Autowired
    public DKGSecurityService(DKGConnector dkgConnector) {
        this.dkgConnector = dkgConnector;
    }

    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    Calendar today;

    public boolean checkLicense(String licenseOwner1, String licenseOwner2) throws ParseException {

        if (StringUtils.isBlank(licenseOwner1) || StringUtils.isBlank(licenseOwner2)) return false;

        ArrayList<Graph> slList1 = querySecurityLicense(licenseOwner1);
        if (slList1 == null) return false;

        ArrayList<Graph> slList2 = querySecurityLicense(licenseOwner2);
        if (slList2 == null) return false;

        ArrayList<OtObject> otObjectListLicense1 = findLicenses(slList1, licenseOwner1);
        if (CollectionUtils.isEmpty(otObjectListLicense1)) return false;

        ArrayList<OtObject> otObjectListLicense2 = findLicenses(slList2, licenseOwner2);
        if (CollectionUtils.isEmpty(otObjectListLicense2)) return false;

        return validateLicenses(otObjectListLicense1) && validateLicenses(otObjectListLicense2);
    }


    private ArrayList<Graph> querySecurityLicense(String licenseOwner) {
        ArrayList<Graph> arrayList = dkgConnector.sendDKGQueryForSecurityLicense(licenseOwner);
        if (arrayList != null && arrayList.size() > 0) {
            return arrayList;
        }
        return null;
    }

    private ArrayList<OtObject> findLicenses(ArrayList<Graph> slOtObjectArray, String license) {
        ArrayList<OtObject> otObjectList = new ArrayList<>();
        for (Graph graph : slOtObjectArray) {
            if (graph != null) {
                OtObject otObject = graph.getOtObject();
                if (otObject != null) {
                    for (Identifier identifier : otObject.getIdentifiers()) {
                        if (identifier != null) {
                            if (StringUtils.containsIgnoreCase(IdentifierTypes.LICENSE.toString(), identifier.identifierType)
                                    && dkgConnector.checkDatasetValidity(graph.getDatasets().iterator().next())) {
                                String owner = otObject.getProperties().getOwner();
                                if (StringUtils.equalsIgnoreCase(owner, license)) {
                                    otObjectList.add(otObject);
                                }
                            }
                        }
                    }
                }
            }
        }
        return otObjectList;
    }

    private boolean validateLicenses(ArrayList<OtObject> otObjectList) throws ParseException {
        for (OtObject otObject : otObjectList) {
            today = Calendar.getInstance();

            Calendar checkedDate = Calendar.getInstance();
            checkedDate.setTime(sdf.parse(otObject.getProperties().getValidity()));

            boolean checkResult = today.before(checkedDate);
            if (checkResult) {
                return true;
            }
        }
        return false;
    }
}
