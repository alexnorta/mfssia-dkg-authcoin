package com.example.mfssia.model.datasetinfo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.ArrayList;
import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DatasetInfoRoot {

    @JsonProperty("data_hash")
    public String dataHash;
    @JsonProperty("data_provider_wallets")
    public ArrayList<DataProviderWallet> dataProviderWallets;
    @JsonProperty("dataset_id")
    public String datasetId;
    @JsonProperty("import_time")
    public Date importTime;
    @JsonProperty("otjson_size_in_bytes")
    public int otjsonSizeInBytes;
    @JsonProperty("replication_info")
    public Object replicationInfo;
    @JsonProperty("root_hash")
    public String rootHash;
    @JsonProperty("total_graph_entities")
    public int totalGraphEntities;

}
