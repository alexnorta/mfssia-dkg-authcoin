package com.example.mfssia.model.datasetinfo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataProviderWallet {

    @JsonProperty("blockchain_id")
    public String blockchainId;
    public String wallet;
}
