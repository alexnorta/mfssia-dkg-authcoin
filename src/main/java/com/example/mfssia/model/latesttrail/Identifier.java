package com.example.mfssia.model.latesttrail;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Identifier {

    @JsonProperty("@type")
    public String identifierType;
    @JsonProperty("@value")
    public String identifierValue;
}
