package com.example.mfssia.model.latesttrail;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.ArrayList;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DKGResponseRoot {

    @JsonProperty("graph")
    public ArrayList<Graph> graphList;
}
