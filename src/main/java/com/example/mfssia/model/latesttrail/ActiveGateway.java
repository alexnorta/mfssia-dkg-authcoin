package com.example.mfssia.model.latesttrail;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ActiveGateway {

    @Schema(description = "producerNetwork", defaultValue = "Example(must leave only 1): ethereum or polygon or hyperledger")
    public String producerNetwork;
    @Schema(description = "consumerNetwork", defaultValue = "Example(must leave only 1): ethereum or polygon or hyperledger")
    public String consumerNetwork;
}
