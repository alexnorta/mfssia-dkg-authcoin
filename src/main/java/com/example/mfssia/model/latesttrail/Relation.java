package com.example.mfssia.model.latesttrail;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Relation {

    @JsonProperty("@type")
    public String type;
    public LinkedObject linkedObject;
    public Properties properties;
}
