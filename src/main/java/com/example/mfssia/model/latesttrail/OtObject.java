package com.example.mfssia.model.latesttrail;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.ArrayList;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OtObject {

    @JsonProperty("@id")
    public String id;
    @JsonProperty("@type")
    public String type;
    public ArrayList<Identifier> identifiers;
    public Properties properties;
    public ArrayList<Relation> relations;
}

