package com.example.mfssia.model.latesttrail;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.ArrayList;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Properties {

    //common properties
    public String timestamp;
    public String signature;

    //system properties
    @JsonProperty("network")
    public String network;
    @JsonProperty("contract_id")
    public ArrayList<String> contractId;

    //Contract properties
    @JsonProperty("consumer_network")
    public String consumerNetwork;
    @JsonProperty("producer_network")
    public String producerNetwork;
    @JsonProperty("price")
    public int price;
    @JsonProperty("quantity")
    public int quantity;
    @JsonProperty("delivery_interval")
    public int deliveryInterval;
    @JsonProperty("product_name")
    public String productName;

    //security license
    public String owner;
    public String issuer;
    public String validity;

    //network properties
    @JsonProperty("activeGateways")
    public ArrayList<ActiveGateway> activeGateways;


    public String relationType;

}
