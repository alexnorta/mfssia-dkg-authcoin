package com.example.mfssia.model.latesttrail;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LinkedObject {

    @JsonProperty("@id")
    public String id;
}
