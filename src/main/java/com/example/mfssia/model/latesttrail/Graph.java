package com.example.mfssia.model.latesttrail;

import lombok.*;

import java.util.ArrayList;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Graph {

    public ArrayList<String> datasets;
    public OtObject otObject;
}
