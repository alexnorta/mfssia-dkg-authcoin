package com.example.mfssia.model.publish;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContractData {

    @Schema(description = "ID of the contract", defaultValue = "1234")
    public String contractNumber;
    @Schema(description = "producerNetwork", defaultValue = "Example(must leave only 1): ethereum or polygon or hyperledger")
    public String producerNetwork;
    @Schema(description = "producerNetwork", defaultValue = "Example(must leave only 1): ethereum or polygon or hyperledger")
    public String consumerNetwork;
    public String price;
    public String quantity;
    public String deliveryInterval;
    public String productName;
}
