package com.example.mfssia.model.publish;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SystemData {

    @Schema(description = "ID of the system", defaultValue = "1234")
    public String systemNumber;
    @Schema(description = "Network", defaultValue = "Example(must leave only 1): ethereum or polygon or hyperledger")
    public String network;
}
