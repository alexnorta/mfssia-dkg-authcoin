package com.example.mfssia.model.publish;

import com.example.mfssia.model.latesttrail.ActiveGateway;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.ArrayList;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GatewayData {

    @Schema(description = "Array of consumer and producer networks")
    public ArrayList<ActiveGateway> activeGateways;
}
