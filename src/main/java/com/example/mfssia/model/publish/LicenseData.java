package com.example.mfssia.model.publish;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LicenseData {

    @Schema(description = "End date of license", defaultValue = "1234ABC")
    public String licenseNumber;
    @Schema(description = "ID of the system/owner", defaultValue = "1234")
    public String systemNumber;
    @Schema(description = "Name of the issuer", defaultValue = "Some name")
    public String issuer;
    @Schema(description = "End date of license", defaultValue = "23-05-2024")
    public String validity;
}
