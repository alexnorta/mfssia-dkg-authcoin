package com.example.mfssia.configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerOpenApiConfig {

    @Bean
    public OpenAPI customOpenAPI(@Value("${application-version}")
                                         String appVersion) {
        return new OpenAPI()
                .info(new Info()
                        .title("MFSSIA Rest API")
                        .version(appVersion)
                        .description("This application purpose is to serve as a bridge between smart contract oracles and DKG nodes")
                        .termsOfService("http://swagger.io/terms/")
                        .license(new License().
                                name("Apache 2.0").
                                url("http://springdoc.org")));
    }
}
