package com.example.mfssia.api;

import com.example.mfssia.model.publish.ContractData;
import com.example.mfssia.service.DKGInitialSetupService;
import com.example.mfssia.service.DKGPublishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/api/setup")
@CrossOrigin(exposedHeaders="Access-Control-Allow-Origin")
public class SetupController {

    private final DKGInitialSetupService dkgInitialSetupService;

    @Autowired
    public SetupController(DKGInitialSetupService dkgInitialSetupService) {
        this.dkgInitialSetupService = dkgInitialSetupService;
    }

    @GetMapping("firstSetup")
    @ResponseBody
    public String initializeFirstSetup() throws IOException {
        return dkgInitialSetupService.publishFiles();
    }

    @GetMapping("healthcheck")
    @ResponseBody
    public String healthCheck() {
        return "Healthcheck OK";
    }

}
