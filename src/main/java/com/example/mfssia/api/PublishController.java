package com.example.mfssia.api;

import com.example.mfssia.model.publish.ContractData;
import com.example.mfssia.model.publish.GatewayData;
import com.example.mfssia.model.publish.LicenseData;
import com.example.mfssia.model.publish.SystemData;
import com.example.mfssia.service.DKGPublishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/api/publish")
@CrossOrigin(exposedHeaders="Access-Control-Allow-Origin")
public class PublishController {

    private final DKGPublishService dkgContractService;

    @Autowired
    public PublishController(DKGPublishService dkgPublishService) {
        this.dkgContractService = dkgPublishService;
    }

    @PostMapping("contract")
    @ResponseBody
    public String publishContract(@RequestBody ContractData contractData) throws IOException {
        return dkgContractService.publishContractData(contractData);
    }

    @PostMapping("system")
    @ResponseBody
    public String publishSystem(@RequestBody SystemData systemData) throws IOException {
        return dkgContractService.publishSystemData(systemData);
    }

    @PostMapping("security")
    @ResponseBody
    public String publishLicense(@RequestBody LicenseData licenseData) throws IOException {
        return dkgContractService.publishLicenseData(licenseData);
    }

    @PostMapping("gateway")
    @ResponseBody
    public String publishGateway(@RequestBody GatewayData gatewayData) throws IOException {
        return dkgContractService.publishGateways(gatewayData);
    }
}
