package com.example.mfssia.api;

import com.example.mfssia.service.DKGContractService;
import com.example.mfssia.service.DKGGatewayService;
import com.example.mfssia.service.DKGSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.ParseException;

@RestController
@RequestMapping("/api/consensus")
public class ConsensusController {

    private final DKGContractService dkgContractService;
    private final DKGSecurityService dkgSecurityService;
    private final DKGGatewayService dkgGatewayService;

    @Autowired
    public ConsensusController(DKGContractService dkgContractService, DKGSecurityService dkgSecurityService, DKGGatewayService dkgGatewayService) {
        this.dkgContractService = dkgContractService;
        this.dkgSecurityService = dkgSecurityService;
        this.dkgGatewayService = dkgGatewayService;
    }

    @GetMapping("contract")
    @ResponseBody
    public String checkContractConsensus(@RequestParam(name = "contractId") String contractId) {
        return dkgContractService.getContractHash(contractId);
    }

    @GetMapping("gateway")
    @ResponseBody
    public boolean checkGatewayConsensus(@RequestParam(name = "contractId") String contractId) {
        return dkgGatewayService.checkGateways(contractId);
    }

    @GetMapping("security")
    @ResponseBody
    public boolean checkSecurityConsensus(@RequestParam(name = "ownerId1") String ownerId1, @RequestParam(name = "ownerId2") String ownerId2) throws ParseException {
        return dkgSecurityService.checkLicense(ownerId1, ownerId2);
    }
}
